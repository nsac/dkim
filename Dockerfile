FROM alpine:latest

LABEL maintainer="Richard Brinkman <server@nsac.alpenclub.nl>"

EXPOSE 8891

VOLUME /var/db/dkim

RUN apk --no-cache add logrotate opendkim opendkim-utils

COPY entrypoint.sh /usr/bin/
COPY internalhosts keytable opendkim.conf signingtable /etc/opendkim/

ENTRYPOINT ["/usr/bin/entrypoint.sh"]

CMD ["opendkim", "-l"]
