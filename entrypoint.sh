#!/bin/sh

# Generate signing keys if they don't exist
if [ ! -f /var/db/dkim/default.private ]
then
	opendkim-genkey --directory=/var/db/dkim --domain=alpenclub.nl --selector=default
	echo "Add the following to your DNS entries:"
	cat /var/db/dkim/default.txt
fi

for s in "esac" ibex lsac nsac rsac tsac
do
	if [ ! -f /var/db/dkim/$s.private ]
	then
		opendkim-genkey --directory=/var/db/dkim --domain=$s.alpenclub.nl --selector=$s
		echo "Add the following to your DNS entries:"
		cat /var/db/dkim/$s.txt
	fi
done

syslogd

exec $@
